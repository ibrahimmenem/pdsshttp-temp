#ifndef HTTPHANDLER_H 
#define HTTPHANDLER_H

#include <map>
#include <string>
#include <unistd.h> /* read and write*/
#include <iostream>
#include <fstream>

class HttpHandler {

public:

	HttpHandler(int fd,std::map<std::string,std::string> config ); 
	int handle(void);

private:
	std::map<std::string,std::string> config_;
    int fd_;
	/* Static member will be defined only once, for all instances of the class*/  
	typedef std::map<std::string, std::string> MimeType;
    static MimeType fillMimetype();
    static MimeType mimeTypes_;

};

#endif
