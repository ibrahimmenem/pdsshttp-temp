/* TODO TBD Copyrights and licensing:
 *
 */

/* Description: 
 * Simple implementation of low level HTTP server 
 */

/* Style: 
 * 1. Use tabs instead of spaces (recommended size 4) 
 * 2. Stay within 80 columns width
 * 3. Avoid or minimize the use of single line comments "//" 
 * 4. Use name space in cpp files not in header files  
 * TODO
 */

/* Includes  */
#include "pdsshttp.h"

using namespace std;

/* Global variables  */
string Helpstring = "***Bad arguments***\n\nUsage:\n \
\t./pdsshttp configfile.cfg";

	/*initialize mimetypes static member (map)
	must be declared in class but defined outside class and main */
HttpHandler::MimeType HttpHandler::mimeTypes_(HttpHandler::fillMimetype());

/* main() function */
int main(int argc, char* argv[])
{
	/*Check input*/
    if (argc !=2)
	{
		cout <<  Helpstring << endl;
		exit (EXIT_FAILURE);
	}

    string fileName(argv[1]);
	/* Instantiate parser object and parse configuration file*/
	ConfigParser C(fileName); 
	map <string,string> config=C.parse();

	/* Print loaded configurations*/
	map<string,string>::iterator i;
	cout<<"Loaded configuration:"<< endl;
	for(i=config.begin(); i != config.end(); ++i) 
		cout << i->first <<":"<<setw(30-(i->first.length()))<<i->second<< " "<<endl;
	cout << endl;


	/*Run one handler instant*/
	int fd=44;
	HttpHandler H(fd,config);
	H.handle();
	return 0;

}
