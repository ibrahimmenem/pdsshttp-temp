#include "httphandler.h"
#define BUFFERSIZE 4096



#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <string.h>

using namespace std;


 
HttpHandler::MimeType HttpHandler::fillMimetype()
{
    MimeType ret;
	ret["html"]="text/html";
	ret["htm"]="text/html";
	ret["jpeg"]="image/jpeg";
	ret["jpg"]="image/jpg";
	ret["gif"]="image/gif";
	ret["png"]="image/png";
    return ret;
}



HttpHandler::HttpHandler(int fd,map<string,string> config ){

	fd_=fd;
	config_=config;


} 

int 
HttpHandler::handle(void){
/*
	cout<<mimeTypes_["jpeg"]<<endl;
	cout<<config_["server.port"]<<endl;
	int readresult;
	char buffer[BUFFERSIZE+1];
	readresult=read(fd_,buffer,BUFFERSIZE); 
	buffer[readresult]=0;
	for(i=0;i<readresult;i++) // Removing CF and LF from message  
		if(buffer[i] == '\r' || buffer[i] == '\n')
			buffer[i]=' ';

	for(i=4;i<BUFFERSIZE;i++) {  
		if(buffer[i] == ' ') {  
			buffer[i] = 0;
			break;
		}
	}
*/


/* temporal socket stuff*/
int socketfd,portno,n,n2,i,newfd,ret,indexfd;
long len;
socklen_t clilen;
char buffer[4096];
struct sockaddr_in serv_addr, cli_addr;
socketfd = socket(AF_INET, SOCK_STREAM, 0);
portno = atoi(config_["server.port"].c_str());
serv_addr.sin_family = AF_INET;
serv_addr.sin_addr.s_addr = INADDR_ANY;//arreglar
serv_addr.sin_port = htons(portno);
int optval=1;
setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
if (bind(socketfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	cout<<"ERROR on binding"<<endl;
listen(socketfd,5);
clilen = sizeof(cli_addr);
newfd=accept( socketfd,(struct sockaddr *) &cli_addr, &clilen );


n2 = read(newfd,buffer,4095);


std::string filename = (config_["root.directory"]+"/index.html");
indexfd = open(filename.c_str() , O_RDONLY  );

len = (long)lseek(indexfd, (off_t)0, SEEK_END);
	  (void)lseek(indexfd, (off_t)0, SEEK_SET); 

 

(void)sprintf(buffer,"HTTP/1.1 200 OK\nServer: pdsshttp/0.0\nContent-Length: %ld\nConnection: close\nContent-Type: %s\n\n",len, mimeTypes_["html"].c_str()); 
fprintf(stderr,"HEADER: %s\n",buffer);


(void)write(newfd,buffer,strlen(buffer));

while (	(ret = read(indexfd, buffer, 4095)) > 0 ) {
fprintf(stderr,buffer);
		(void)write(newfd,buffer,ret);
	}

 
sleep(1);
close(newfd);

/* fin temporal socket stuff*/



return 0;
} 
