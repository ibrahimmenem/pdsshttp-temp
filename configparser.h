#ifndef CONFIGPARSER_H 
#define CONFIGPARSER_H


#include <map>
#include <string>


class ConfigParser {

public:

	ConfigParser(std::string fileName);
	std::map<std::string,std::string> parse(void);
	std::string get_cfgFileName();
   
private:
    std::string  cfgFileName_;
};

#endif
