CC=g++
CFLAGS=-g -Werror -I.
DEPS = configparser.h pdsshttp.h httphandler.h
BINARIES=pdsshttp

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

pdsshttp: pdsshttp.o configparser.o httphandler.o
	g++ -o pdsshttp pdsshttp.o configparser.o httphandler.o -I.

.PHONY: clean

clean:
	rm -f $(BINARIES) *.o
